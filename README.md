# NetAcademia-Android-elso

Videó során használt linkek

Global
-Android dashboard https://developer.android.com/about/dashboards/
-Activity lifecycle https://www.javatpoint.com/images/androidimages/Android-Activity-Lifecycle.png
-Random user API https://randomuser.me/documentation#results
-Hiba megoldás https://github.com/square/okhttp/issues/3180

Libek
-Support libek https://developer.android.com/topic/libraries/support-library/packages
-Constraint layout https://developer.android.com/training/constraint-layout/
-Butterknife https://github.com/JakeWharton/butterknife
-Glide https://github.com/bumptech/glide
-Retrofit- moshi https://github.com/square/retrofit/blob/master/retrofit-converters/moshi/README.md
-Moshi https://github.com/square/moshi
-Retrofit- RxJava2 https://github.com/square/retrofit/tree/master/retrofit-adapters/rxjava2
-RxAndroid https://github.com/ReactiveX/RxAndroid

Guide oldalak
-BindViewholder metodus https://developer.android.com/reference/android/support/v7/widget/RecyclerView.Adapter#bindviewholder
-Butterknife guide page http://jakewharton.github.io/butterknife/