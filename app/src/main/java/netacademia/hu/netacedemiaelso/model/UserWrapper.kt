package netacademia.hu.netacedemiaelso.model

import com.squareup.moshi.Json

/**
 * TODO: Add a class header comment!
 */
class UserWrapper(
        @Json(name = "results")
        var users: List<User>
)