package netacademia.hu.netacedemiaelso.model

import com.squareup.moshi.Json

/**
 * TODO: Add a class header comment!
 */
class ProfilePicture(
        @Json(name = "large")
        var picture: String?
)