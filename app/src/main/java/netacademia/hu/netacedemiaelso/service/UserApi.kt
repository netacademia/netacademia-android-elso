package netacademia.hu.netacedemiaelso.service

import io.reactivex.Observable
import netacademia.hu.netacedemiaelso.model.UserWrapper
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * TODO: Add a class header comment!
 */
interface UserApi {

    @GET("api/")
    fun getUsers(@Query("results") count: Int): Observable<UserWrapper>
}