package netacademia.hu.netacedemiaelso.service

/**
 * TODO: Add a class header comment!
 */
interface HandleResponse<in T> {
    fun onResponse(response: T)
    fun onError(error: Throwable)
}